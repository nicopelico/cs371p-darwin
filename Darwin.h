#ifndef Darwin_h
#define Darwin_h

#include <vector>
#include <string>
#define west 0
#define north 1
#define east 2
#define south 3

class Species{
	private:
		std::vector<std::string> instructions;
	public:
		//----------------
		//darwin_species constructor
		//----------------
		/**
		* string constructor for species
		* @param a string
		*/
		Species(std::string);
		
		//~Species();
		//----------------
		//darwin_addInstruction
		//----------------
		/**
		* adds an instruction to a species' program
		* @param string that is instruction
		*/
		void addInstruction(std::string);
		//----------------
		//species_getInstruction
		//----------------
		/**
		* returns the instruction pointed to by an index
		* @param int i the index of the instruction
		* @return string representation of instruction
		*/
		std::string getInstruction(int);		//returns instruction at the index of the parameter
		//----------------
		//species_numInstructions
		//----------------
		/**
		* returns the number of instructions for a species
		* @return number of intructions
		*/
		int numInstructions();					//returns number of instructions
		
		void printProgram();		//no unit tests
	
};

class Creature{
	private:
		char symbol;
		int direction;
		int programCounter;
		Species* theSpecies;
		bool eligibleFlag;
		
	public:
		//----------------
		//creature_constructor
		//----------------
		/**
		* constructs an instance of a creature
		* @param species pointer
		* @param int for direction
		* @param char for symbol
		*/
		Creature(Species*, int, char);		//change to references??
		//Creature(Creature&);	//copy constructor		i dont use this i believe
		//----------------
		//creature_eligible
		//----------------
		/**
		* tells whehter a creature is eligible for a turn
		* @return true if eligible, false otherwise
		*/
		bool eligible();
		//----------------
		//creature_toggleEligibility
		//----------------
		/**
		* toggles the state of eligibility for a creature
		*/
		void toggleEligibility();
		//----------------
		//creature_nextInstruction
		//----------------
		/**
		* returns the instruction pointed to by the program counter
		* @return string representation of instruction
		*/
		std::string nextInstruction();		//returns instruction based on program counter
		//----------------
		//creature_peekInstruction
		//----------------
		/**
		* returns next instruction without incrementing the program counter
		* @return string representation of instruction
		*/
		std::string peekInstruction();		//returns instruction without incrementing program counter
		//----------------
		//creature_modPC
		//----------------
		/**
		* changes the program counter
		* @param int that is new program counter
		*/
		void modPC(int);
		//----------------
		//creature_incrementPC
		//----------------
		/**
		* increments the PC by 1
		*/
		void incrementPC();
		//----------------
		//creatur_frontSpot
		//----------------
		/**
		* calculates the coordinates of the spot in front of a creature
		* @param int row
		* @param int col
		* @return returns a pair of integers representing the row,col of the spot in front
		*/
		std::pair<int, int> frontSpot(int, int);		//row, col
		//----------------
		//creature_turnLeft
		//----------------
		/**
		* turns a creature to the left
		*/
		void turnLeft();
		//----------------
		// creature_turnRight
		//----------------
		/**
		* turns a creature to the right
		*/
		void turnRight();
		//----------------
		//creature_getInfected
		//----------------
		/**
		* infects the current creature to the type of the parameter. changes species and resets program counter. Nothing else changes.
		* @param creature pointer that is the infecting creature
		*/
		void getInfected(Creature*);
		void printInfo();				//no unit tests
		//----------------
		//creature_toSymbol
		//----------------
		/**
		* returns a character representation of a creature
		*/
		char toSymbol();
};

class Darwin{
	private:
		std::vector<std::vector<Creature*> > board;		//change to creature for inner vector later
		//----------------
		//darwin_executeControl
		//----------------
		/**
		* executes the control instruction of a creature
		* @param string that is instruction
		* @param creature pointer that points to creaturea that instruction belongs to
		* @param int row
		* @param int col
		*/
		void executeControl(std::string, Creature*, int, int);		//executes the control actions of creatures. These control instructions rely on information that is in the Darwin class
		//----------------
		//darwin_executeAction
		//----------------
		/**
		* executes the action instruction of a creature
		* @param string that is instruction
		* @param creature pointer that points to creaturea that action belongs to
		* @param int row
		* @param int col
		*/
		void executeAction(std::string, Creature*, int, int);
	public:
		//----------------
		//darwin_constructor (int, int)
		//----------------
		/**
		* int, int constructor for darwin class. Creates a board that has first_int rows and second_int cols
		* @param int number of rows
		* @param int number of cols
		*/
		Darwin(int, int);		//int int constructor
		//----------------
		//darwin_addCreature
		//----------------
		/**
		* adds a creature to the board
		* @param creature pointer to creature being added
		* @param int for row
		* @param int for col
		*/
		void addCreature(Creature*, int, int);
		//----------------
		//darwin_simulate
		//----------------
		/**
		* simulates darwin game
		* @param int number of rounds
		* @param ofstream& output file
		*/
		void simulate(int, std::ofstream&);				//only number of rounds so far. think about how to deal with which grids to print
		//----------------
		//darwin_printBoard
		//----------------
		/**
		* prints the board
		* @param ofstream& output file
		*/
		void printBoard(std::ofstream&);

};

#endif //Darwin_h
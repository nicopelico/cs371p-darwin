
//g++ -pedantic -std=c++0x -Wall Darwin.c++ Darwin.h test.c++ -o Darwin
#include "Darwin.h"
#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <fstream>

Darwin::Darwin(int w, int l){
using namespace std;
	for(int i = 0; i < w; i++){
		vector<Creature*> temp;
		for(int j = 0; j < l; j++){
			Creature* tempCreature = 0;
			temp.push_back(tempCreature);					//initializing with integers for testing purposes
		}
		board.push_back(temp);
	}
}

void Darwin::addCreature(Creature* myCreature, int row, int col){
	board[row][col] = myCreature;
}

void Darwin::printBoard(std::ofstream& output){
using namespace std;
	output << "  ";
	for(int i = 0; (unsigned)i < board[0].size(); i++)
		output << i%10 << " ";
	output << endl;
	for(int i = 0; (unsigned)i < board.size(); i++){
		for(int j = 0; (unsigned)j < board[0].size(); j++){
			if(j == 0)
				output << i%10 << " ";
			if(board[i][j] == 0)
				output << ". ";
			else
				output << board[i][j]->toSymbol() << " ";
		}
		output << endl;
	}
	output << endl;
}
/**
** Simulate
**/
void Darwin::simulate(int numRounds, std::ofstream& output){
using namespace std;
	
	bool action = false;
	bool eligibleToggle = true;
	string instruction;
	string actionInstruction;
	for(int round = 0; round < numRounds; round++){
		output << "ROUND " << round << endl;
		for(int i = 0; (unsigned)i < board.size(); i++){
			for(int j = 0; (unsigned)j < board[0].size(); j++){
				if(board[i][j] != 0 && eligibleToggle == board[i][j]->eligible()){
					board[i][j]->toggleEligibility();
					while(!action){
						instruction = board[i][j]->nextInstruction();
						// if action instruction
						if(instruction.compare("hop") == 0 || instruction.compare("left") == 0 || instruction.compare("right") == 0 || instruction.compare("infect") == 0){
							actionInstruction = instruction;
							executeAction(actionInstruction, board[i][j], i, j);
							action = true;
						}
						//else if control instruction
						else{
							executeControl(instruction, board[i][j], i, j);
						}
						
					}
					action = false;
				}
				
			}
		}
	printBoard(output);
	eligibleToggle = !eligibleToggle;
	}

}

/**
** executeAction
**/
void Darwin::executeAction(std::string action, Creature* currCreature, int row, int col){
using namespace std;
	//cout << "executing action " << action << endl;
	pair<int, int> front = currCreature->frontSpot(row, col);
	if(action.compare("hop") == 0){
		if(front.first >= 0 && front.second >= 0 && (unsigned)front.first < board.size() && (unsigned)front.second < board[0].size()){
			if(board[front.first][front.second] == 0){
				board[front.first][front.second] = currCreature;
				board[row][col] = 0;
			}
			
		}
		
	}
	else if(action.compare("left") == 0){
		currCreature->turnLeft();
	}
	else if(action.compare("right") == 0){
		currCreature->turnRight();
	}
	//infect
	else{
		//if there is an enemy in front
		if(board[front.first][front.second] != 0 && currCreature->toSymbol() != board[front.first][front.second]->toSymbol()){
			board[front.first][front.second]->getInfected(currCreature);
		}
	}
}

/**
** executeControl
**/
void Darwin::executeControl(std::string instruction, Creature* currCreature, int row, int col){
using namespace std;
	pair<int, int> coord;		//row, col
	//execute the control instruction for the current creature
	//check instruction against all 5 control instructions. if the predicate is true go to line n, otherwise go to next line. use modPC for this.
	if(instruction.compare(0, 8, "if_empty") == 0){
		coord = currCreature->frontSpot(row, col);
		//if facing wall 
		if(coord.first < 0 || coord.second < 0 || (unsigned)coord.first == board.size() || (unsigned)coord.second == board[0].size()){
			//continue to next instruction
		}
		else if(board[coord.first][coord.second] != 0){
			//space occupied, continue to next instruction
		}
		else{
			//go to line n
			currCreature->modPC(instruction.back() - '0');
		}
	}
	else if(instruction.compare(0, 7, "if_wall") == 0){
		coord = currCreature->frontSpot(row, col);
		//if facing wall
		if(coord.first < 0 || coord.second < 0 || (unsigned)coord.first == board.size() || (unsigned)coord.second == board[0].size()){
			//go to line n
			currCreature->modPC(instruction.back() - '0');
		}
		else{
			//continue to next instruction
		}
	}
	else if(instruction.compare(0, 9, "if_random") == 0){
		coord = currCreature->frontSpot(row, col);
		if(rand()%2 == 0){
			//continue to next instruction
		}
		else{
			//go to line n
			currCreature->modPC(instruction.back() - '0');
		}
	}
	else if(instruction.compare(0, 8, "if_enemy") == 0){
		coord = currCreature->frontSpot(row, col);
		//if facing wall 
		if(coord.first < 0 || coord.second < 0 || (unsigned)coord.first == board.size() || (unsigned)coord.second == board[0].size()){
			//continue to next instruction
		}
		else if(board[coord.first][coord.second] == 0){
			//space is empty, continue to next instruction
		}
		//else, if enemy
		else if(currCreature->toSymbol() != board[coord.first][coord.second]->toSymbol()){
			//go to line n
			currCreature->modPC(instruction.back() - '0');
		}
		else{
			//continue to next instruction
			//currCreature->incrementPC();
		}
	}
	else{		//go instruction
		coord = currCreature->frontSpot(row, col);
		currCreature->modPC(instruction.back() - '0');
		
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Species::Species(std::string type) : instructions(0){
	if(type.compare("food") == 0){
		instructions.push_back("left");
		instructions.push_back("go 0");
	}
	else if(type.compare("hopper") == 0){
		instructions.push_back("hop");
		instructions.push_back("go 0");
	}
	else if(type.compare("rover") == 0){
		instructions.push_back("if_enemy 9");
		instructions.push_back("if_empty 7");
		instructions.push_back("if_random 5");
		instructions.push_back("left");
		instructions.push_back("go 0");
		instructions.push_back("right");
		instructions.push_back("go 0");
		instructions.push_back("hop");
		instructions.push_back("go 0");
		instructions.push_back("infect");
		instructions.push_back("go 0");
	}
	else if(type.compare("trap") == 0){
		instructions.push_back("if_enemy 3");
		instructions.push_back("left");
		instructions.push_back("go 0");
		instructions.push_back("infect");
		instructions.push_back("go 0");
	}
	//throw exception here?? no valid type given (act like default constructor for now)
}



void Species::addInstruction(std::string instruction){
using namespace std;
	instructions.push_back(instruction);
}

std::string Species::getInstruction(int index){
	return instructions[index];
}

int Species::numInstructions(){
	return instructions.size();
}

void Species::printProgram(){
using namespace std;
	for(int i = 0; (unsigned)i < instructions.size(); i++){
		//cout << instructions[i] << endl;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Creature::Creature(Species* theSpecies, int direction, char mySymbol){
	symbol = mySymbol;
	programCounter = 0;
	eligibleFlag = true;
	this->direction = direction;
	this->theSpecies = theSpecies;
}

bool Creature::eligible(){
	return eligibleFlag;
}

void Creature::toggleEligibility(){
	eligibleFlag = !eligibleFlag;
}

std::string Creature::nextInstruction(){
	int currentPC = programCounter;
	int numInstructions = theSpecies->numInstructions();
	programCounter = (programCounter + 1)%numInstructions;
	return theSpecies->getInstruction(currentPC);
}

std::string Creature::peekInstruction(){
	return theSpecies->getInstruction(programCounter);
}

void Creature::modPC(int PC){
	programCounter = PC;
}

void Creature::incrementPC(){
	programCounter = (programCounter + 1)%(theSpecies->numInstructions());
}

std::pair<int, int> Creature::frontSpot(int row, int col){
using namespace std;
	pair<int, int> result;
	if(direction == north)
		result = make_pair(row - 1, col);
	else if(direction == east)
		result = make_pair(row, col + 1);
	else if(direction == south)
		result = make_pair(row + 1, col);
	else
		result = make_pair(row, col - 1);
	
	return result;

}

void Creature::turnLeft(){
using namespace std;
	direction = (direction - 1) % 4;
	if(direction == -1)
		direction = 3;
}

void Creature::turnRight(){
	direction = (direction + 1) % 4;
}

void Creature::getInfected(Creature* infecter){
	symbol = infecter->symbol;
	programCounter = 0;
	theSpecies = infecter->theSpecies;
}

void Creature::printInfo(){
using namespace std;
	theSpecies->printProgram();
}

char Creature::toSymbol(){
	return symbol;
}





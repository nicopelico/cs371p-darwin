cs371p-darwin
=============
  This program simulates the game darwin. The game consists of a grid populated with different types of creatures. Each creature has a set of instructions that determines how it moves around the grid and when to infect other creatures. Each creature can execute one action instruction per turn (moving or infecting). 
  This was designed as an API so new creatures can be added and existing ones can be modified.
